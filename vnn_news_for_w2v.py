#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import json
import time
from bs4 import BeautifulSoup
from pymongo import MongoClient


client = MongoClient("mongodb+srv://phamson:qwe12345@cluster0-7taef.mongodb.net/test?retryWrites=true&w=majority")

vnn = 'https://vietnamnet.vn/vn/'
thoi_su = 'thoi-su'
tu_van_suc_khoe = 'suc-khoe/tu-van-suc-khoe/'
suc_khoe_24h = 'suc-khoe/suc-khoe-24h'
chinh_tri = 'chinh-tri'  # 406 pages
kinh_doanh = 'kinh-doanh'  # 1155 pages
giai_tri = 'giai-tri'  # 1052 pages
the_gioi = 'the-gioi'  # 776 pages


def url_to_soup(url):
    source_code = requests.get(url)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text, 'html.parser')
    return soup


def get_relate(url):
    soup = url_to_soup(url)
    relate_section = soup.find_all('div', {'class': "article-relate"})
    related_news_list = []

    if relate_section is not None:
        for rs in relate_section:
            related = rs.find_all('a')
            for r in related:
                related_news = dict()
                related_news['title'] = r.text.strip()
                related_news['url'] = vnn + r.get('href')
                related_news_list.append(related_news)
    return related_news_list


def scrape(start_page, end_page, category, collection):
    db = client.vtv_news
    col = collection
    collection = db[col]

    data = []
    news_category = category + '/'
    page = start_page

    # for the 5 news that just there
    url = vnn + news_category
    print(url)
    soup = url_to_soup(url)

    # got block or smt, can't get data from some pages, top_1 and top_content still work
    # comment out top_1 and top_content to reduce duplicate records

    top_1 = soup.find('div', {'class': 'top-one-cate'}).find('h3', {'class': 'm-t-5'}).find('a')
    top_1_news = dict()
    top_1_news['url'] = vnn + top_1.get('href')
    top_1_news['title'] = top_1.text.strip()
    top_1_news['related_news'] = get_relate(top_1_news['url'])
    top_1_news['category']  = category

    top_1_soup = url_to_soup(top_1_news['url'])
    top_1_content = top_1_soup.find('div', {'class': 'ArticleContent'})
    top_1_news['content'] = ''

    if top_1_news is not None:
        news_p = top_1_content.find_all('p')
        for p in news_p:
            top_1_news['content'] += p.text.strip()
    collection.insert_one(top_1_news)

    top_content = soup.find('div', {'class': "BoxCate BoxStyle5"}).find("ul", {'class': 'height-list va-top'})
    for li in top_content.find_all('li', {'class': 'clearfix'}):
        news = dict()
        news['url'] = vnn + li.find('a').get('href')
        news['title'] = li.find('a').get('title')
        news['related_news'] = get_relate(news['url'])
        news['category'] = category

        news_soup = url_to_soup(news['url'])
        news_content = news_soup.find('div', {'class': 'ArticleContent'})
        news['content'] = ''

        if news_content is not None:
            news_p = news_content.find_all('p')
            for p in news_p:
                news['content'] += p.text.strip()
        collection.insert_one(news)
    # ----------------------------------------------------------------

    while page <= end_page:
        url = vnn + news_category + 'trang' + str(page)
        soup = url_to_soup(url)

        content = soup.find('div', {'class': 'list-content list-content-loadmore lagre m-t-20 clearfix'})
        while content is None:
            print("no data on page {0}, re-scraping".format(page))
            time.sleep(1)
            content = soup.find('div', {'class': 'list-content list-content-loadmore lagre m-t-20 clearfix'})
            if content is not None:
                print("got it")

        for div in content.find_all('div', {'class': 'clearfix item'}):
            news = dict()
            news['url'] = vnn + div.find('a').get('href')
            news['title'] = div.find('a').get('title')
            news['related_news'] = get_relate(news['url'])
            news['category'] = category

            news_soup = url_to_soup(news['url'])
            news_content = news_soup.find('div', {'class': 'ArticleContent'})
            news['content'] = ''

            if news_content is not None:
                news_p = news_content.find_all('p')
                for p in news_p:
                    news['content'] += p.text.strip()

            collection.insert_one(news)

        # with open("vnn_with_url_1538p.json", 'w') as json_data_out:
        #     json.dump(data, json_data_out, ensure_ascii=False, indent=2)
        print("done page {0}".format(page))
        page += 1


# scrape(1563, thoi_su)
scrape(213, 776, the_gioi, 'the_gioi')
# test(18)

# the gioi loi tu trang 202, 201 da xonog
